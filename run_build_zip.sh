#!/bin/bash

# assuming you're running bash and have Java and ANT-1.6.2 or up installed more or less the way they should be

if [ -f /usr/libexec/java_home ]; then
export JAVA_HOME=$(/usr/libexec/java_home)
else
  # detect java location
  if [ -z "${JAVA_HOME+zzz}" ] || [ "${JAVA_HOME+xxx}" = "xxx" ]; then
    for JAVA_HOME in /usr/lib/jvm/java-8-oracle /usr/lib/jvm/java-7-oracle /usr/lib/jvm/java /usr/lib/jvm/default-java /usr/lib/jvm/java-6-sun /System/Library/Frameworks/JavaVM.framework/Home
    do
        if [ -d "$JAVA_HOME" ]; then break ; fi
    done
  fi
fi
export JAVA_HOME

if [ "${JAVA_HOME}" = "" ] || [ ! -d "$JAVA_HOME" ] ; then
  echo "Failed to detect JAVA_HOME location, we cannot proceed ($JAVA_HOME)."
  exit 1
fi


MYCWD=`pwd`
# save the orig classpath if any (you shouldn't need one at all)
CLASSPATHORIG=$CLASSPATH
# add the libs needed for ANT to run, we have to do this because of the ANT taskdef
CLASSPATH=$CLASSPATH:$MYCWD/lib/bcel.jar:$MYCWD/lib/javac2.jar:$MYCWD/lib/jdom.jar
export CLASSPATH
ant
ant package
# set the classpath back
CLASSPATH=$CLASSPATHORIG
export CLASSPATH
