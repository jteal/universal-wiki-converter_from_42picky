
This is a fully open-source fork of the UWC that aims on 
developing this tool by the community.


AppFusions does not provide an issue tracker nor 
it does maintain the repository. 

We aim to fix this by keeping this freely 
accessible and allowing others to contribute.

Still, we cannot guarantee that we will fix 
reported bugs, but we promise to integrate 
pull requests and to give write access to other people.